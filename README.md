## Advent Of Code

This is a fun solution of [Advent of Code](https://adventofcode.com/), do not 
take it seriously as I have only focused on solution's rather then 
optimising memory/speed. Feel free to understand the solutions, and make 
your own solution. **Cheers**!!