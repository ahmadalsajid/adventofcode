def part_1():
    with open('input.txt', 'r') as _file:
        _sum = 0
        for line in _file:
            _card_no, _numbers = line.split(':')
            _winning_numbers, _numbers_i_have = _numbers.strip().split('|')
            _winning_set = [int(_i) for _i in _winning_numbers.strip().split(' ') if _i]
            _my_numbers = [int(_i) for _i in _numbers_i_have.strip().split(' ') if _i]
            _common = len(list(set(_winning_set) & set(_my_numbers)))
            if _common > 0:
                _sum = _sum + pow(2, _common - 1)
        print(_sum)


def part_2():
    with open('input.txt', 'r') as _file:
        data = [line for line in _file]
        cards = [1] * len(data)
        for i, line in enumerate(data):
            _card_no, _numbers = line.split(':')
            _winning_numbers, _numbers_i_have = _numbers.strip().split('|')
            _winning_set = [int(_i) for _i in _winning_numbers.strip().split(' ') if _i]
            _my_numbers = [int(_i) for _i in _numbers_i_have.strip().split(' ') if _i]
            _common = len(list(set(_winning_set) & set(_my_numbers)))
            for j in range(1, _common + 1):
                cards[i + j] = cards[i + j] + cards[i]
        print(sum(cards))


def main():
    part_1()
    part_2()


if __name__ == '__main__':
    main()
