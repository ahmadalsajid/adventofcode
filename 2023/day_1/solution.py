def part_1():
    with open('input.txt', 'r') as _file:
        _sum = 0
        for line in _file:
            _input = [i for i in line if i.isnumeric()]
            _sum = _sum + int(_input[0] + _input[-1])
        print(_sum)


def part_2():
    with open('input.txt', 'r') as _file:
        _sum = 0

        _words = {
            'one': '1',
            'two': '2',
            'three': '3',
            'four': '4',
            'five': '5',
            'six': '6',
            'seven': '7',
            'eight': '8',
            'nine': '9'
        }
        # for each line, run a loop
        for line in _file:
            _number = []
            while line:
                if line[0].isdigit():
                    _number.append(line[0])
                    line = line[1:]
                    continue
                # for each word, run a loop
                for key, value in _words.items():
                    if line.startswith(key):
                        _number.append(value)
                        line = line[1:]
                    continue
                line = line[1:]
            _sum = _sum + int(_number[0] + _number[-1])
        print(_sum)


def main():
    part_1()
    part_2()


if __name__ == '__main__':
    main()
