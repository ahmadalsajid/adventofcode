def part_1():
    with open('input.txt', 'r') as _file:
        _total_ball_set = {
            'red': 12,
            'green': 13,
            'blue': 14
        }
        _sum = 0

        for line in _file:
            # get the game numer first

            line = line.strip('\n')
            _game_number, _game_sets = line.split(':')
            _games = _game_sets.split(';')
            impossible = False
            for _game in _games:
                _ball_sets = _game.split(',')
                # print(_ball_sets)
                _temp = {}
                for _ball_set in _ball_sets:
                    _n, _color = _ball_set.strip().split(' ')
                    if int(_n) > _total_ball_set[_color]:
                        impossible = True

            if not impossible:
                _input = [i for i in _game_number if i.isnumeric()]
                _sum = _sum + int(''.join(_input))

        print(_sum)


def part_2():
    with open('input.txt', 'r') as _file:
        _sum = 0
        for line in _file:
            _total_ball_set = {
                'red': 1,
                'green': 1,
                'blue': 1
            }
            line = line.strip('\n')
            _game_number, _game_sets = line.split(':')
            _games = _game_sets.split(';')
            for _game in _games:
                _ball_sets = _game.split(',')
                # print(_ball_sets)
                _temp = {}
                for _ball_set in _ball_sets:
                    _n, _color = _ball_set.strip().split(' ')
                    if int(_n) > _total_ball_set[_color]:
                        _total_ball_set[_color] = int(_n)
            result = 1
            for key, value in _total_ball_set.items():
                result = result * value
            _sum = _sum + result
        print(_sum)


def main():
    part_1()
    part_2()


if __name__ == '__main__':
    main()
