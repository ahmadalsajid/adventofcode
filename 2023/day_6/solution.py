def part_1():
    with open('input.txt', 'r') as _file:
        _lines = _file.readlines()
        _multiply = 1
        _vector = []
        _time = [int(_i) for _i in _lines[0].strip().split(':')[1].split()]
        _record = [int(_i) for _i in _lines[1].strip().split(':')[1].split()]
        _multiply = 1
        for _indx, _step in enumerate(_time):
            _sum = 0
            _mid = _step // 2 + 1 if _step % 2 else _step // 2
            for _i in range(1, _mid + 1):
                _distance = _i * (_step - _i)
                if _distance > _record[_indx]:
                    _sum += 1
            _sum = (_sum - 1) * 2 if _step % 2 else _sum * 2 - 1
            _multiply = _multiply * _sum
        print(_multiply)


def part_2():
    with open('input.txt', 'r') as _file:
        _lines = _file.readlines()
        _multiply = 1
        _vector = []
        _time = int(''.join([_i for _i in _lines[0].strip().split(':')[1].split()]))
        _record = int(''.join([_i for _i in _lines[1].strip().split(':')[1].split()]))
        _multiply = 1
        _sum = 0
        _mid = _time // 2 + 1 if _time % 2 else _time // 2
        for _i in range(1, _mid + 1):
            _distance = _i * (_time - _i)
            if _distance > _record:
                _sum += 1
        _sum = (_sum - 1) * 2 if _time % 2 else _sum * 2 - 1
        _multiply = _multiply * _sum
        print(_multiply)


def main():
    part_1()
    part_2()


if __name__ == '__main__':
    main()
