def first_solution():
    measurements = []
    counter = 0
    with open('input.txt', 'r') as _file:
        for line in _file:
            measurements.append(int(line))

        for i in range(len(measurements) - 1):
            if measurements[i + 1] > measurements[i]:
                counter = counter + 1

        print(counter)


def second_solution():
    measurements = []
    cumulative_sum = []
    counter = 0
    with open('input.txt', 'r') as _file:
        for line in _file:
            measurements.append(int(line))

        for i in range(len(measurements) - 2):
            cumulative_sum.append(measurements[i] + measurements[i + 1] + measurements[i + 2])
        for i in range(len(cumulative_sum) - 1):
            if cumulative_sum[i + 1] > cumulative_sum[i]:
                counter = counter + 1

        print(counter)


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
