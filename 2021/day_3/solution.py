from pprint import pprint
from collections import Counter


def first_solution():
    _matrix = []
    with open('input.txt', 'r') as _file:
        for line in _file:
            _matrix.append(list(line.strip()))

        _transformed_matrix = list(zip(*_matrix))
        # pprint(_transformed_matrix)
        _gamma_bits = []
        _epsilon_bits = []
        for _ in _transformed_matrix:
            _counter = dict(Counter(_))
            # pprint(_counter)
            if _counter['0'] > _counter['1']:
                _gamma_bits.append('0')
                _epsilon_bits.append('1')
            else:
                _gamma_bits.append('1')
                _epsilon_bits.append('0')

        print(int(''.join(_gamma_bits), 2) * int(''.join(_epsilon_bits), 2))


def second_solution():
    with open('input.txt', 'r') as _file:
        for line in _file:
            pass


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
