def first_solution():
    _horizontal = _depth = 0
    with open('input.txt', 'r') as _file:
        for line in _file:
            _direction, _steps = line.strip('\n').split()
            if _direction == 'forward':
                _horizontal = _horizontal + int(_steps)
            elif _direction == 'down':
                _depth = _depth + int(_steps)
            elif _direction == 'up':
                _depth = _depth - int(_steps)

        print(_horizontal * _depth)


def second_solution():
    _horizontal = _depth = _aim = 0
    with open('input.txt', 'r') as _file:
        for line in _file:
            _direction, _steps = line.strip('\n').split()
            if _direction == 'forward':
                _horizontal = _horizontal + int(_steps)
                _depth = _depth + (_aim * int(_steps))
            elif _direction == 'down':
                # _depth = _depth + int(_steps)
                _aim = _aim + int(_steps)
            elif _direction == 'up':
                # _depth = _depth - int(_steps)
                _aim = _aim - int(_steps)
            # print(_horizontal, _depth, _aim)

        print(_horizontal * _depth)


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
