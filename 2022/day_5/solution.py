from pprint import pprint

matrix_dict = {}


def split_line(a, n):
    # split string into substrings containing max n characters
    chunks, chunk_size = len(a), 4
    _list = [a[i:i + chunk_size] for i in range(0, chunks, chunk_size)]
    if len(_list) < 9:
        _list.extend(['    '] * (9 - len(_list)))
    return _list


def rearrange_containers_first_solution(_no_of_container, _source, _destination):
    # get the source stack
    _source_stack = matrix_dict[_source]
    # split the containers to be removed
    _to_be_moved = _source_stack[len(_source_stack) - int(_no_of_container):]
    # set source stack's new value
    matrix_dict[_source] = _source_stack[:len(_source_stack) - int(_no_of_container)]
    # set the new destination stack
    _destination_stack = matrix_dict[_destination]
    matrix_dict[_destination] = _destination_stack + _to_be_moved[::-1]


def first_solution():
    global matrix_dict
    matrix_dict = {}
    with open('input.txt', 'r') as _file:
        _matrix = []
        for line_number, line in enumerate(_file):
            line = line.strip('\n')
            # print(line_number, line)
            if line_number < 9:
                _matrix.append(split_line(line, 4))
                pass
            elif line_number == 9:
                # prepare a dictionary with first element as key and only the elements as stack element
                _new_matrix = list(list(x)[::-1] for x in zip(*_matrix))
                # pprint(_new_matrix)
                for _list in _new_matrix:
                    matrix_dict[_list[0].replace(' ', '')] = ''.join(
                        [_elem.replace(' ', '').replace('[', '').replace(']', '') for _elem in _list[1:]])
                # print(matrix_dict)
            else:
                # move stack element as instructed
                _, _no_of_container, _, _source, _, _destination = line.split()
                rearrange_containers_first_solution(_no_of_container, _source, _destination)

        print(matrix_dict)
        _top = []
        for key, value in matrix_dict.items():
            if value:
                _top.append(value[-1])
        print(''.join(_top))


def rearrange_containers_second_solution(_no_of_container, _source, _destination):
    # get the source stack
    _source_stack = matrix_dict[_source]
    # split the containers to be removed
    _to_be_moved = _source_stack[len(_source_stack) - int(_no_of_container):]
    # set source stack's new value
    matrix_dict[_source] = _source_stack[:len(_source_stack) - int(_no_of_container)]
    # set the new destination stack
    _destination_stack = matrix_dict[_destination]
    matrix_dict[_destination] = _destination_stack + _to_be_moved


def second_solution():
    global matrix_dict
    matrix_dict = {}
    with open('input.txt', 'r') as _file:
        _matrix = []
        for line_number, line in enumerate(_file):
            line = line.strip('\n')
            # print(line_number, line)
            if line_number < 9:
                _matrix.append(split_line(line, 4))
                pass
            elif line_number == 9:
                # prepare a dictionary with first element as key and only the elements as stack element
                _new_matrix = list(list(x)[::-1] for x in zip(*_matrix))
                # pprint(_new_matrix)
                for _list in _new_matrix:
                    matrix_dict[_list[0].replace(' ', '')] = ''.join(
                        [_elem.replace(' ', '').replace('[', '').replace(']', '') for _elem in _list[1:]])
                # print(matrix_dict)
            else:
                # move stack element as instructed
                _, _no_of_container, _, _source, _, _destination = line.split()
                rearrange_containers_second_solution(_no_of_container, _source, _destination)

        print(matrix_dict)
        _top = []
        for key, value in matrix_dict.items():
            if value:
                _top.append(value[-1])
        print(''.join(_top))


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
