def main():
    elf_calories = []

    with open('input.txt', 'r') as _file:
        single_inventory = []
        for line in _file:
            _input = line.strip('\n')
            if _input == '':
                elf_calories.append(sum(single_inventory))
                single_inventory = []
            else:
                single_inventory.append(int(_input))
        # for the last batch of entries
        elf_calories.append(sum(single_inventory))

        # part 1 answer
        print(max(elf_calories))

        # part 2 answer
        # print(elf_calories.sort(reverse=True))
        print(sum(sorted(elf_calories, reverse=True)[:3]))


if __name__ == '__main__':
    main()
