def first_solution():
    with open('input.txt', 'r') as _file:
        total_priority = 0
        for line in _file:
            mid = len(line) // 2
            _first, _second = line[:mid], line[mid:]
            _item = list(set(_first).intersection(_second))[0]
            if _item.islower():
                total_priority = total_priority + ord(_item) - 96
            else:
                total_priority = total_priority + ord(_item) - 38
        print(total_priority)


def split_line(a, n):
    # split string into substrings containing max n characters
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def get_common_item(_sacks):
    return set(_sacks[0]).intersection(_sacks[1]).intersection(set(_sacks[1]).intersection(_sacks[2]),
                                                               set(_sacks[2]).intersection(_sacks[0]))


def second_solution():
    with open('input.txt', 'r') as _file:
        _all_lines = []
        for line in _file:
            _all_lines.append(line.strip('\n'))

        chunks, chunk_size = len(_all_lines), 3
        _sacks = [_all_lines[i:i + chunk_size] for i in range(0, chunks, chunk_size)]
        total_priority = 0
        for _sack in _sacks:
            _item = list(get_common_item(_sack))[0]
            if _item.islower():
                total_priority = total_priority + ord(_item) - 96
            else:
                total_priority = total_priority + ord(_item) - 38
        print(total_priority)


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
