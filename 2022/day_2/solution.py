"""
Part 1
Opponent:Me:
A:X: Rock        : 1
B:Y: Paper       : 2
C:Z: Scissors    : 3

Lost : 0
Draw : 3
Win  : 6

A>Z
B>X
C>Y

Part 2

X: Loose
Y: Draw
Z: Win

"""

my_choice = {
    'X': 1,
    'Y': 2,
    'Z': 3
}

opponent_choice = {
    'A': 1,
    'B': 2,
    'C': 3
}

result_points = {
    'win': 6,
    'draw': 3,
    'lost': 0
}


def match_result(_opponent, _me):
    if (_opponent == 'A' and _me == 'X') or (_opponent == 'B' and _me == 'Y') or (_opponent == 'C' and _me == 'Z'):
        return result_points['draw'] + my_choice[_me]

    if (_opponent == 'A' and _me == 'Y') or (_opponent == 'B' and _me == 'Z') or (_opponent == 'C' and _me == 'X'):
        return result_points['win'] + my_choice[_me]
    else:
        return result_points['lost'] + my_choice[_me]


def part_1():
    with open('input.txt', 'r') as _file:
        total_score = 0
        for line in _file:
            _opponent, _me = line.split()
            _result = match_result(_opponent, _me)
            total_score = total_score + _result

        # part 1 solution
        print(total_score)


def win_move(_opponent):
    if _opponent == 'A':
        return 'B'
    elif _opponent == 'B':
        return 'C'
    else:
        return 'A'


def loose_move(_opponent):
    if _opponent == 'A':
        return 'C'
    elif _opponent == 'B':
        return 'A'
    else:
        return 'B'


def fix_match(_opponent, _choice):
    if _choice == 'X':
        return result_points['lost'] + opponent_choice[loose_move(_opponent)]
    elif _choice == 'Y':
        return result_points['draw'] + opponent_choice[_opponent]
    elif _choice == 'Z':
        return result_points['win'] + opponent_choice[win_move(_opponent)]


def part_2():
    with open('input.txt', 'r') as _file:
        total_score = 0
        for line in _file:
            _opponent, _choice = line.split()
            _result = fix_match(_opponent, _choice)
            total_score = total_score + _result

        # part 2 solution
        print(total_score)


def main():
    part_1()
    part_2()


if __name__ == '__main__':
    main()
