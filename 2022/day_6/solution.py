# N sized substrings with K distinct characters

def find_n_substring_k_distinct_chars(line, N, K):
    line = line.strip('\n')
    for _index in range(0, (len(line) - N + 1)):
        if len(set(line[_index:_index + N])) == K:
            print(_index + N)
            break


def first_solution():
    with open('input.txt', 'r') as _file:
        for line in _file:
            find_n_substring_k_distinct_chars(line, 4, 4)


def second_solution():
    with open('input.txt', 'r') as _file:
        for line in _file:
            find_n_substring_k_distinct_chars(line, 14, 14)


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
