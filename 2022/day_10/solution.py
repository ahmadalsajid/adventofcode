from pprint import pprint


def part_1():
    with open('input.txt', 'r') as _file:
        x = 1
        cycle = 0
        _stats = list()
        for _line in _file:
            _instructions = _line.strip().split(' ')
            # print(f'{_instructions[0]}:::{_instructions[1] if len(_instructions) > 1 else 0}')
            _instruction = _instructions[0]
            _value = None
            if len(_instructions) > 1:
                _value = int(_instructions[1])

            if _instruction == 'noop':
                _stats.append(x)
            else:
                _stats.extend([x, x])
                x += _value
                # _stats.append(x)

            print(f'{_instruction}:::{_value}')
            print(f'{cycle}:::{x}')

        steps = [20, 60, 100, 140, 180, 220]
        _sum = 0
        for step in steps:
            _sum += step * _stats[step - 1]

        for no, val in enumerate(_stats):
            print(f'{no + 1} : {val}')
        print(_sum)


def part_2():
    with open('input.txt', 'r') as _file:
        crt = ['.'] * 240
        x = 1
        cycle = 0
        _stats = list()
        for _line in _file:
            _instructions = _line.strip().split(' ')
            # print(f'{_instructions[0]}:::{_instructions[1] if len(_instructions) > 1 else 0}')
            _instruction = _instructions[0]
            _value = None
            if len(_instructions) > 1:
                _value = int(_instructions[1])

            if _instruction == 'noop':
                crt[cycle] = '#'
            else:
                crt[cycle] = '#'
                cycle += 1
                crt[cycle] = '#'
                x += _value

                for _i in range(3):
                    crt[_i + x] = '#'

                # _stats.append(x)
            cycle += 1

        crt_2d = [crt[i:i + 40] for i in range(0, len(crt), 40)]
        for _d in crt_2d:
            print(''.join(_d))


def main():
    # part_1()
    part_2()


if __name__ == '__main__':
    main()
