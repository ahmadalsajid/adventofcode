def split_by_hyphen(data):
    _first, _second = data.split('-')
    return set(range(int(_first), int(_second) + 1))


def first_solution():
    with open('input.txt', 'r') as _file:
        total_supersets = 0
        for line in _file:
            _first_set, _second_set = line.strip('\n').split(',')
            _first_range = split_by_hyphen(_first_set)
            _second_range = split_by_hyphen(_second_set)

            if _first_range.issuperset(_second_range) or _second_range.issuperset(_first_range):
                total_supersets = total_supersets + 1

        print(total_supersets)


def second_solution():
    with open('input.txt', 'r') as _file:
        total_intersections = 0
        for line in _file:
            _first_set, _second_set = line.strip('\n').split(',')
            _first_range = split_by_hyphen(_first_set)
            _second_range = split_by_hyphen(_second_set)

            if _first_range.intersection(_second_range):
                total_intersections = total_intersections + 1
        print(total_intersections)


def main():
    first_solution()
    second_solution()


if __name__ == '__main__':
    main()
